import {lookupTokenType, Token, TokenType} from './token'

export class Lexer {
    private source: string;
    private position: number;
    private readPosition: number;
    private character: string;
    
    constructor(source: string) {
        this.source = source;
        this.position = 0;
        this.readPosition = 0;
        this.character= '';
        this._readChar();
    }

    _readChar() {
        if (this.readPosition >= this.source.length) {
            this.character = null;
        } else {
            this.character = this.source[this.readPosition];
        }
        this.position = this.readPosition;
        this.readPosition += 1;
    }

    nextToken(): Token {
        let token: Token;
        if (this.character.match(/^=$/)) {
            token = new Token(TokenType.ASSIGN, this.character);
        } else if (this.character.match(/^\+$/)) {
            token = new Token(TokenType.PLUS, this.character);
        } else if (this.character.match(/^-$/)) {
            token = new Token(TokenType.MINUS, this.character);
        } else if (this.character.match(/^\(/)) {
            token = new Token(TokenType.LEFT_PARENTESIS, this.character);
        } else if (this.character.match(/^\)$/)) {
            token = new Token(TokenType.RIGHT_PARENTESIS, this.character);
        } else if (this.character.match(/^\{$/)) {
            token = new Token(TokenType.LEFT_BRACKET, this.character);
        } else if (this.character.match(/^\}$/)) {
            token = new Token(TokenType.RIGHT_BRACKET, this.character);
        } else if (this.character.match(/^;$/)) {
            token = new Token(TokenType.SEMICOLON, this.character);
        } else if (this.character.match(/^,$/)) {
            token = new Token(TokenType.COMMA, this.character);
        } else if (this._isLetter(this.character)) {
            var literal = this._readIdentifier();
            var type = lookupTokenType(literal);
        }

        else {
            token = new Token(TokenType.ILLEGAL, this.character);
        }

        this._readChar();
        return token;
    }

    _readIdentifier() {
        let position = this.position;
        while (this._isLetter(this.character)) {
            this._readChar();
        }
        return this.source.slice(position, this.position);
    }

    _isLetter(char: string) {
        return char.match(/^[a-zA-Z]$/);
    }

    _isDigit(char: string) {
        return char.match(/^[0-9]$/);
    }



    _skipWhitespace() {
        while (this.character && this.character.match(/^\s$/)) {
            this._readChar();
        }
    }
}