
export enum TokenType {
    ASSIGN,
    COMMA,
    EOF,
    FUNCTION,
    IDENTIFIER,
    ILLEGAL,
    INT,
    LEFT_BRACKET,
    VARIABLE,
    LEFT_PARENTESIS,
    PLUS,
    RIGHT_BRACKET,
    RIGHT_PARENTESIS,
    SEMICOLON,
    MINUS,
}

export class Token {
    type: TokenType;
    literal: any
    constructor(type: TokenType, literal: any) {
        this.type = type;
        this.literal = literal;
    }
}

export const lookupTokenType = (literal: any) => {
    var keywords = {
        'fun': TokenType.FUNCTION,
        'var': TokenType.VARIABLE,
    }

    return keywords[literal] || TokenType.IDENTIFIER;

}